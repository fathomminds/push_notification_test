<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use App\Models\TokenModel;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, TokenModel $tokenModel = null)
    {
        if ($tokenModel === null) {
            $tokenModel = new TokenModel;
        }
        $token = $tokenModel->get($request);
        if (empty($token)) {
            return new JsonResponse('Unauthorized', 401);
        }
        try {
            $jwt = $tokenModel->decode($token, config('jwt.secret'), config('jwt.allowed_algs'));
            $this->autoRefresh($jwt);
            return $next($request);
        } catch (\Exception $ex) {
            return new JsonResponse('Unauthorized', 401);
        }
    }

    private function autoRefresh($jwt, $time = null)
    {
        $time = $time === null ? time() : $time;
        $expiresIn = $jwt->exp - $time;
        if ($expiresIn < config('jwt.auto_refresh_threshold')) {
            $tokenModel = new TokenModel;
            $token = $tokenModel->issue($jwt->uid, $jwt->iss, config('jwt.exp_type_session'), $time);
            $tokenModel->setCookie($token);
        }
    }
}
