<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\UserModel;
use App\Models\TokenModel;

class AuthController extends Controller
{

    public function auth(Request $request) {
        try {
            $response = [
                'accessToken' => 'access_token_' . rand(1000000, 9999999),
                'accessTokenValidUntil' => 14000555444,
                'requestToken' => 'refresh_token_' .  rand(1000000, 9999999),
                'requestTokenValidUntil' => 24000555444,
            ];
            return new JsonResponse($response);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    /**
     * Issues am authentication token
     *
     * @param UserModel $userModel
     * @return JsonResponse
     */
    public function login(UserModel $userModel, TokenModel $tokenModel, Request $request)
    {
        try {
            $input = json_decode($request->getContent());
            $token = $userModel->login($input->email, $input->password);
            $tokenModel->setCookie($token);
            return new JsonResponse(['jwt_token' => $token]);
        } catch (\Exception $ex) {
	    var_dump($ex->getMessage());
            return new JsonResponse('Unauthorized', 401);
        }
    }

    /**
     * Returns the available authentication token
     *
     * @return JsonResponse
     */
    public function check(TokenModel $model, Request $request)
    {
        try {
            $token = $model->get($request);
            $jwt = $model->decode($token, config('jwt.secret'), config('jwt.allowed_algs'));
            return new JsonResponse(['decoded_jwt_token' => $jwt]);
        } catch (\Exception $ex) {
            return new JsonResponse(['error' => $ex->getMessage()]);
        }
    }
}
