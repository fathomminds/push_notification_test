<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\DevicesModel;
use App\Models\SnsModel;
use Fathomminds\Rest\Exceptions\RestException;

class DevicesController extends Controller
{
    public function set(Request $request, DevicesModel $model)
    {
        try {
            $input = json_decode($request->getContent());
            $resource = new \StdClass;
            $resource->name = $input->name;
            $resource->deviceId = $input->deviceId;
            $resource->platform = $input->platform;
            $model->resource($resource);
            if ($input->platform === 'android') {
                $model->setGoogleEndpoint();
            } elseif ($input->platform === 'ios') {
                $model->setAppleEndpoint();
            }
            $model->create();
            return new JsonResponse($model->resource());
        } catch (RestException $ex) {
            if ($ex->getMessage() === 'Unique constraint violation') {
                $list = $model->all();
                foreach ($list as $resource) {
                    if ($resource->name === $input->name) {
                        error_log(print_r($resource, 1));
                        $resource->name = $input->name;
                        $resource->deviceId = $input->deviceId;
                        $resource->platform = $input->platform;
                        $model->resource($resource);
                        if ($input->platform === 'android') {
                            $model->setGoogleEndpoint();
                        } elseif ($input->platform === 'ios') {
                            $model->setAppleEndpoint();
                        }
                        $model->update();
                        return new JsonResponse($resource);
                    }
                }
            }
            return new JsonResponse($ex->getMessage(), 500);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function delete(Request $request, DevicesModel $model)
    {
        try {
            $resource = json_decode($request->getContent());
            $id = $resource->_id;
            $model->one($id)->delete();
            return new JsonResponse($resource);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function list(DevicesModel $model)
    {
        try {
            return new JsonResponse($model->all());
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function send(Request $request, SnsModel $sns)
    {
        try {
            $input = json_decode($request->getContent());
            $messageId = $sns->publish($input);
            return new JsonResponse(['messageId' => $messageId]);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }
}
