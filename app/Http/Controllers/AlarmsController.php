<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\DevicesModel;

class AlarmsController extends Controller
{
    public function listAlarms(Request $request)
    {
        try {
            $input = json_decode($request->getContent(), true);
            $repeated = $request->header("x-repeated-request");
            if ($repeated) {
                $proxy = new ProxyController;
                $res = $proxy->proxy($request);
                return new JsonResponse(json_decode($res->content()));
            }
            return new JsonResponse($input, 401);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }
}
