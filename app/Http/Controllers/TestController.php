<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Fathomminds\Rest\Exceptions\RestException;

class TestController extends Controller
{
    public function testGet(Request $request)
    {
        sleep(2);
        $params = $request->all();
        foreach ($params as $key => $value) {
            $params[$key] = intval($value);
        }
        $requestParams = array_merge(
            [
                'method' => 'get',
                'isAuthenticated' => false,
                'token' => $request->bearerToken(),
            ],
            $params
        );
        file_put_contents("test.txt", json_encode($requestParams) . "\n", FILE_APPEND);
        return new JsonResponse($requestParams);
    }

    public function testPost(Request $request)
    {
        $requestParams = [
            'method' => 'post',
            'isAuthenticated' => false,
            'token' => $request->bearerToken(),
        ];
        return new JsonResponse(array_merge(
            $requestParams,
            json_decode($request->getContent(), true)
        ));
    }

    public function testPut(Request $request)
    {
        $requestParams = [
            'method' => 'put',
            'isAuthenticated' => true,
            'token' => $request->bearerToken(),
        ];
        return new JsonResponse(array_merge(
            $requestParams,
            json_decode($request->getContent(), true)
        ));
    }

    public function testAuthenticatedGet(Request $request)
    {
        sleep(2);
        $params = $request->all();
        foreach ($params as $key => $value) {
            $params[$key] = intval($value);
        }
        $requestParams = array_merge(
            [
                'method' => 'get',
                'isAuthenticated' => true,
                'token' => $request->bearerToken(),
            ],
            $params
        );
        file_put_contents("test.txt", json_encode($requestParams) . "\n", FILE_APPEND);
        return new JsonResponse($requestParams);
    }

    public function testAuthenticatedPost(Request $request)
    {
        $requestParams = [
            'method' => 'post',
            'isAuthenticated' => true,
            'token' => $request->bearerToken(),
        ];
        return new JsonResponse(array_merge(
            $requestParams,
            json_decode($request->getContent(), true)
        ));
    }

    public function testAuthenticatedPut(Request $request)
    {
        $requestParams = [
            'method' => 'put',
            'isAuthenticated' => false,
            'token' => $request->bearerToken(),
        ];
        return new JsonResponse(array_merge(
            $requestParams,
            json_decode($request->getContent(), true)
        ));
    }

    public function auth(Request $request)
    {
        try {
            $input = json_decode($request->getContent());
            $token = $request->bearerToken();
            if ($token !== 'VALID') {
                $response = [];
                return new JsonResponse($response, 401);
            }
            $t = file_get_contents('/tmp/token.txt');
            $response = [
              'accessToken' => $t,
              'accessTokenValidUntil' => time() + 999999,
              'requestToken' => $t,
              'requestTokenValidUntil' => time() + 999999,
            ];
            return new JsonResponse($response);
        } catch (RestException $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function mandown(Request $request)
    {
        file_put_contents(
            '/tmp/mandown.txt',
            date("Y-m-d H:i:s") . " - " . $request->getContent() . "\n",
            FILE_APPEND | LOCK_EX
        );
        return new JsonResponse([]);
        // return new JsonResponse('Internal Server Error', 500);
    }

    public function register(Request $request, $deviceId)
    {
        try {
            $input = json_decode($request->getContent());
            $response = [
              'deviceId' => 'whatever',
              'status' => 'REGISTERED',
              'accessToken' => 'VALID',
              'accessTokenValidUntil' => time() - 1,
              'requestToken' => 'VALID',
              'requestTokenValidUntil' => time() - 1,
            ];
            return new JsonResponse($response);
        } catch (RestException $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    private $alarms = [
      // '999900',
      // '999901',
      // '999902',
      // '999903',
      // '999904',
      // '999905',
      // '999906',
      // '999907',
      // '999908',
      // '999909',
      // '999910',
      // '999911',
      // '999912',
      // '999913',
      // '999914',
      // '999915',
      // '999916',
      // '999917',
      // '999918',
      // '999919',
    ];

    public function list(Request $request)
    {
        $alarms = [];
        foreach ($this->alarms as $id) {
            $alarms[] = $this->readAlarm($id);
        }
        return new JsonResponse([
          'alarms' => $alarms
        ]);
    }

    private $alarmTemplates = [
        // '777700',
        // '777701',
        // '777702',
        // '777703',
    ];

    public function listTemplates(Request $request)
    {
        $alarmTemplates = [];
        foreach ($this->alarmTemplates as $id) {
            $alarmTemplates[] = $this->readAlarmTemplate($id);
        }
        return new JsonResponse([
          'alarm_templates' => $alarmTemplates
        ]);
    }

    public function setAllStatus(Request $request)
    {
        $input = json_decode($request->getContent());
        $status = (string)$input->status;
        foreach ($this->alarms as $id) {
            $this->saveAlarm($id, $status);
        }
        return new JsonResponse('OK');
    }

    private function readAlarm($id)
    {
        return json_decode(file_get_contents("/data/www/bmgate.fathomminds.com/app/alarms/{$id}.json"), 1);
    }

    private function readAlarmTemplate($id)
    {
        return json_decode(file_get_contents("/data/www/bmgate.fathomminds.com/app/alarm_templates/{$id}.json"), 1);
    }

    private function saveAlarm($id, $status = 'seen')
    {
        $alarm = $this->readAlarm($id);
        $alarm['status'] = $status;
        $alarm['updatedAt'] = (string)time();
        file_put_contents(
            "/data/www/bmgate.fathomminds.com/app/alarms/{$id}.json",
            json_encode($alarm, JSON_PRETTY_PRINT)
        );
    }

    public function setSeen(Request $request)
    {
        try {
            $input = json_decode($request->getContent());
            $id = (string)$input->id;
            $status = (string)$input->status;
            $this->saveAlarm($id, $status);
            $alarm = $this->readAlarm($id);
            return new JsonResponse($alarm);
        } catch (RestException $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function errorLog(Request $request)
    {
        file_put_contents(
            '/tmp/error-log.txt',
            date("Y-m-d H:i:s") . " - " . $request->getContent() . "\n",
            FILE_APPEND | LOCK_EX
        );
        return new JsonResponse([]);
    }

    public function newAlarm(Request $request) {
        return new JsonResponse(null);
    }

    public function config(Request $request)
    {
        $response = json_decode(
            file_get_contents('/data/www/bmgate.fathomminds.com/app/Http/Controllers/testconfig.json'),
            true
        );
        return new JsonResponse($response);
    }

    public function hb()
    {
        return new JsonResponse([
            "status" => "ok"
        ]);
    }
}
