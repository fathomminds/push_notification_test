<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\DevicesModel;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        try {
            $input = json_decode($request->getContent());
            $deviceController = new DevicesController();
            return $deviceController->set($request, new DevicesModel);
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return new JsonResponse($ex->getMessage(), 500);
        }
    }

    public function registerProxied(Request $request)
    {
        try {
            $input = json_decode($request->getContent());
            error_log(print_r($input, 1));
            // $deviceController = new DevicesController();
            // $deviceController->set($request, new DevicesModel);
            $proxy = new ProxyController;
            $res = $proxy->proxy($request);
            error_log(print_r($res, 1));
            return $res;
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return new JsonResponse($ex->getMessage(), 500);
        }
    }
}
