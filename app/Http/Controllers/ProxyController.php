<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Fathomminds\Rest\Exceptions\RestException;

class ProxyController extends Controller
{
    public function proxy(Request $request)
    {
        try {
            $targetApi = getenv("PROXIED_API");
            $method = $request->method();
            $root = $request->root();
            $targetUrl = str_replace($root, $targetApi, str_replace('v1/', '', $request->fullUrl()));
            $body = $request->getContent();
            $headers = [];
            foreach($request->header() as $key => $value) {
                switch ($key) {
                    case 'content-type':
                        $key = 'Content-Type';
                        $headers[] = $key . ': ' . $value[0];
                        break;
                    case 'authorization':
                        $key = 'Authorization';
                        $headers[] = $key . ': ' . $value[0];
                        break;
                }
            }
            $ch = curl_init($targetUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
            if ($body !== "") {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            };
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            error_log(print_r($headers, 1));
            error_log(print_r($body, 1));
            $result = curl_exec($ch);
            error_log(print_r($result, 1));
            return new JsonResponse(json_decode($result));
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }
}
