<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\DevicesModel;

class MandownController extends Controller
{
    public function send(Request $request)
    {
        try {
            $input = json_decode($request->getContent(), true);
            error_log(print_r($input, 1));
            $proxy = new ProxyController;
            $res = $proxy->proxy($request);
            return new JsonResponse(json_decode($res->content()));
        } catch (\Exception $ex) {
            return new JsonResponse($ex->getMessage(), 500);
        }
    }
}
