<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class SiteController extends Controller
{

    public function index()
    {
        return view('index', []);
    }
}
