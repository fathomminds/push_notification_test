<?php
namespace App\Models;

use Fathomminds\Rest\RestModel;
use App\Exceptions\CTAException;
use App\Models\Objects\UserObject;
use App\Models\TokenModel;

class UserModel extends RestModel
{
    protected $restObjectClass = UserObject::class;

    public function login($email, $password, TokenModel $tokenModel = null)
    {
        $tokenModel = $tokenModel === null ? new TokenModel : $tokenModel;
        if ($email === 'demo' && $password === 'demo') {
            $token = $tokenModel->issue(
                1,
                config('jwt.issuer'),
                config('jwt.exp_type_session')
            );
            return $token;
        }
        throw new CTAException(trans('userModel.loginFailure'), []);
    }
}
