<?php
namespace App\Models\Objects;

use Fathomminds\Rest\Database\Clusterpoint\RestObject;
use App\Models\Schema\UserSchema;

class UserObject extends RestObject
{
    protected $schemaClass = UserSchema::class;
    protected $resourceName = 'users';
}
