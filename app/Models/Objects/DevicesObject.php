<?php
namespace App\Models\Objects;

use Fathomminds\Rest\Database\Clusterpoint\RestObject;
use App\Models\Schema\DevicesSchema;

class DevicesObject extends RestObject
{
    protected $schemaClass = DevicesSchema::class;
    protected $resourceName = 'devices';
}
