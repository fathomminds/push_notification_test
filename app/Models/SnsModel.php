<?php
namespace App\Models;

use Aws\Sdk;
use Fathomminds\Rest\Helpers\Uuid;

class SnsModel
{
    private function createClient()
    {
        $sdk = new \Aws\Sdk([
            'region'   => getenv('AWS_SDK_REGION'),
            'version'  => getenv('AWS_SDK_VERSION'),
            'http' => [
                'verify' => getenv('AWS_SDK_HTTP_VERIFY') === 'false' ? false : getenv('AWS_SDK_HTTP_VERIFY'),
            ]
        ]);
        return $sdk->createSns();
    }

    public function createGoogleEndpoint($deviceId)
    {
        $client = $this->createClient();
        $result = $client->createPlatformEndpoint([
            'PlatformApplicationArn' => env('AWS_SNS_PLATFORM_APPLICATION_ARN_GOOGLE'),
            'Token' => $deviceId,
        ]);
        return $result;
    }

    public function publish($params)
    {
        $device = $params->device;
        $payload = $params->payload;
        if (isset($device->platform)) {
            if ($device->platform === 'android') {
                return $this->sendGCM($device->endpoint, $payload);
            }
            if ($device->platform === 'ios') {
                return $this->sendAPNS($device->endpoint, $payload);
            }
        }
    }

    private function sendGCM($endpointArn, $payload)
    {
        $message = json_encode([
            'GCM' => json_encode($payload->GCM),
        ]);
        $client = $this->createClient();
        $res = $client->publish([
            'TargetArn' => $endpointArn,
            'MessageStructure' => 'json',
            'Message' => $message,
        ]);
        return $res['MessageId'];
    }

    public function createAppleEndpoint($deviceId)
    {
        $client = $this->createClient();
        $result = $client->createPlatformEndpoint([
            'PlatformApplicationArn' => env('AWS_SNS_PLATFORM_APPLICATION_ARN_APPLE'),
            'Token' => $deviceId,
        ]);
        return $result;
    }

    private function sendAPNS($endpointArn, $payload)
    {
        $message = json_encode([
            'APNS' => json_encode($payload->APNS),
        ]);
        $client = $this->createClient();
        $res = $client->publish([
            'TargetArn' => $endpointArn,
            'MessageStructure' => 'json',
            'Message' => $message,
        ]);
        return $res['MessageId'];
    }
}
