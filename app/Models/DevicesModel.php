<?php
namespace App\Models;

use Fathomminds\Rest\Exceptions\RestException;
use Fathomminds\Rest\RestModel;
use App\Models\Objects\DevicesObject;
use App\Models\SnsModel;

class DevicesModel extends RestModel
{
    protected $restObjectClass = DevicesObject::class;

    public function setGoogleEndpoint()
    {
        $sns = new SnsModel;
        $result = $sns->createGoogleEndpoint($this->resource()->deviceId);
        $this->resource()->endpoint = $result->get('EndpointArn');
    }

    public function setAppleEndpoint()
    {
        $sns = new SnsModel;
        $result = $sns->createAppleEndpoint($this->resource()->deviceId);
        $this->resource()->endpoint = $result->get('EndpointArn');
    }
}
