<?php
namespace App\Models\Schema;

use Fathomminds\Rest\Schema;
use Fathomminds\Rest\Schema\TypeValidators\StringValidator;
use Fathomminds\Rest\Schema\TypeValidators\AnyValidator;

class DevicesSchema extends Schema
{
    public function schema()
    {
        return [
            '_id' => [
                'validator' => [
                    'class' => StringValidator::class,
                ]
            ],
            'name' => [
                'unique' => true,
                'required' => true,
                'validator' => [
                    'class' => StringValidator::class,
                    'params' => [
                        'maxLength' => 100,
                    ],
                ],
            ],
            'deviceId' => [
                'required' => true,
                'validator' => [
                    'class' => StringValidator::class,
                ],
            ],
            'platform' => [
                'validator' => [
                    'class' => StringValidator::class,
                ],
            ],
            'endpoint' => [
                'validator' => [
                    'class' => AnyValidator::class,
                ],
            ],
            'googleEndpoint' => [
                'validator' => [
                    'class' => AnyValidator::class,
                ],
            ],
        ];
    }
}
