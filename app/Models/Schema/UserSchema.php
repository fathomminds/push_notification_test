<?php
namespace App\Models\Schema;

use Fathomminds\Rest\Schema;
use Fathomminds\Rest\Schema\TypeValidators\StringValidator;

class UserSchema extends Schema
{
    public function schema()
    {
        return [
            '_id' => [
                'validator' => [
                    'class' => StringValidator::class,
                ]
            ],
            'email' => [
                'unique' => true,
                'required' => true,
                'validator' => [
                    'class' => StringValidator::class,
                    'params' => [
                        'maxLength' => 255,
                    ],
                ],
            ],
            'password' => [
                'validator' => [
                    'class' => StringValidator::class,
                ],
            ],
        ];
    }
}
