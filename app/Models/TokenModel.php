<?php
namespace App\Models;

use Illuminate\Http\Request;
use \Firebase\JWT\JWT;

class TokenModel
{
    public function issue($userId, $issuer = null, $expiresIn = null, $issuedAt = null)
    {
        $issuer = $issuer === null ? config('jwt.issuer') : $issuer;
        $expiresIn = $expiresIn === null ? config('jwt.exp_type_session') : $expiresIn;
        $issuedAt = $issuedAt === null ? time() : $issuedAt;
        $expirationTime = $issuedAt + $expiresIn;
        $token = [
            "iss" => $issuer,
            "iat" => $issuedAt,
            "exp" => $expirationTime,
            "uid" => $userId,
        ];
        return $this->encode($token, config('jwt.secret'));
    }

    public function setCookie($token)
    {
        $cookieJar = app()->make('cookie');
        $cookie = $cookieJar->make(
            config('jwt.cookie_name'),
            $token,
            config('cookie.exp_type_session'),
            config('cookie.path'),
            config('cookie.host'),
            config('cookie.secure'),
            config('cookie.http_only')
        );
        $cookieJar->queue($cookie);
    }

    public function decode($jwt, $key, $allowed_algs = ['HS256'])
    {
        return JWT::decode($jwt, $key, $allowed_algs);
    }

    public function encode($payload, $key, $alg = 'HS256', $keyId = null, $head = null)
    {
        return JWT::encode($payload, $key, $alg, $keyId, $head);
    }

    public function get(Request $request)
    {
        if (null !== $token = $this->getFromCookie($request)) {
            return $token;
        }
        return $this->getFromHeader($request);
    }

    private function getFromCookie(Request $request)
    {
        $token = $request->cookie(config('jwt.cookie_name'));
        if (!empty($token)) {
            return $token;
        }
    }

    private function getFromHeader(Request $request)
    {
        $header = $request->header('authorization');
        if (!empty($header)) {
            return preg_replace('/^Bearer\s+/', '', $header);
        }
    }
}
