<?php
namespace Tests\Models;

use App\Models\UserModel;
use App\Exceptions\CTAException;

class UserModelTest extends \Tests\TestCase
{
    public function testLoginSuccess()
    {
        $model = new UserModel;
        $this->expectException(CTAException::class);
        $token = $model->login('incorrectEmail', 'incorrectPassword');
    }

    public function testLoginFailure()
    {
        $model = new UserModel;
        $token = $model->login('demo', 'demo');
        $this->assertNotNull($token);
    }
}
