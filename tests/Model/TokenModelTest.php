<?php
namespace Tests\Models;

use Illuminate\Http\Request;
use App\Models\TokenModel;
use Mockery;

class TokenModelTest extends \Tests\TestCase
{
    public function testDecodeValid()
    {
        $tokenModel = new TokenModel;
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'.
            'eyJpc3MiOiJJU1NVRVIiLCJpYXQiOiJJU1NVRURBVCIsImV4cCI6MzI1MDM1OTM2MDAsInVpZCI6MX0.'.
            '_cTv_BhuK66qSwwyVQM7P7D4DIbIWToKqWCtSUUXyU4';
        $jwt = $tokenModel->decode($token, 'SECRET');
        $this->assertEquals('ISSUER', $jwt->iss);
        $this->assertEquals('ISSUEDAT', $jwt->iat);
        $this->assertEquals(32503593600, $jwt->exp);
        $this->assertEquals(1, $jwt->uid);
    }

    public function testDecodeExpired()
    {
        $tokenModel = new TokenModel;
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'.
            'eyJpc3MiOiJJU1NVRVIiLCJpYXQiOiJJU1NVRURBVCIsImV4cCI6MSwidWlkIjoxfQ.'.
            'eBbPYlz-rKk4EsDZlXmOHNOk1AFkOlHbA3MoDDCSPSM';
        $this->expectException(\Exception::class);
        $jwt = $tokenModel->decode($token, 'SECRET');
    }

    public function testGetNoToken()
    {
        $tokenModel = new TokenModel;
        $request = new Request([], [], [], [], [], [], null);
        $token = $tokenModel->get($request);
        $this->assertNull($token);
    }

    public function testGetTokenInHeader()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'.
          'eyJpc3MiOiJJU1NVRVIiLCJpYXQiOiJJU1NVRURBVCIsImV4cCI6MzI1MDM1OTM2MDAsInVpZCI6MX0.'.
          '_cTv_BhuK66qSwwyVQM7P7D4DIbIWToKqWCtSUUXyU4';
        $tokenModel = new TokenModel;
        $request = new Request([], [], [], [], [], [], null);
        $request->headers->add([
            'Authorization' => ['Bearer ' . $token],
        ]);
        $retrievedToken = $tokenModel->get($request);
        $this->assertEquals($token, $retrievedToken);
    }

    public function testGetTokenInCookie()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'.
          'eyJpc3MiOiJJU1NVRVIiLCJpYXQiOiJJU1NVRURBVCIsImV4cCI6MzI1MDM1OTM2MDAsInVpZCI6MX0.'.
          '_cTv_BhuK66qSwwyVQM7P7D4DIbIWToKqWCtSUUXyU4';
        $tokenModel = new TokenModel;
        $request = new Request([], [], [], [], [], [], null);
        $request->cookies->add([
            config('jwt.cookie_name') => $token,
        ]);
        $retrievedToken = $tokenModel->get($request);
        $this->assertEquals($token, $retrievedToken);
    }
}
