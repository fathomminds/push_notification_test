<?php
namespace Tests\Middleware;

use App\Exceptions\CTAException;

class DetailedExceptionTest extends \Tests\TestCase
{
    public function testGettingDetails()
    {
        $ex = new CTAException('TEST', ['additionalInfo' => true]);
        $details = $ex->getDetails();
        $this->assertArrayHasKey('additionalInfo', $details);
        $this->assertTrue($details['additionalInfo']);
    }
}
