<?php
namespace Tests;

use Mockery;
use Clusterpoint\Client;
use Clusterpoint\Instance\Service;
use Clusterpoint\Response\Single;
use Clusterpoint\Response\Batch;
use Fathomminds\Rest\Database\Clusterpoint\Database;
use Fathomminds\Rest\Helpers\ReflectionHelper;

abstract class TestCase extends \Laravel\Lumen\Testing\TestCase
{

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    protected $mockDatabase;
    protected $mockClient;

    public function __construct()
    {
        $this->mockDatabase = Mockery::mock(Service::class);
        $this->mockClient = Mockery::mock(Client::class);
        $this->mockClient->shouldReceive('database')->andReturn($this->mockDatabase);
    }

    public function mockResponse($rawResponse, $errors = [])
    {
        $mockResponse = Mockery::mock(Single::class);
        $mockResponse->shouldReceive('error')->andReturn($errors);
        $mockResponse->shouldReceive('rawResponse')->andReturn(json_encode($rawResponse));
        return $mockResponse;
    }

    public function mockBatchResponse($rawResponse, $errors = [])
    {
        $mockResponse = Mockery::mock(Batch::class);
        $mockResponse->shouldReceive('error')->once()->andReturn($errors);
        $mockResponse->shouldReceive('rawResponse')->once()->andReturn(json_encode($rawResponse));
        return $mockResponse;
    }

    public function mockModel($modelClassName = null, $objectClassName = null)
    {
        $ReflectionHelper = new ReflectionHelper;
        $object = $this->mockObject($objectClassName);
        $model = $ReflectionHelper->createInstance($modelClassName, [$object]);
        return $model;
    }

    public function mockObject($objectClassName)
    {
        $ReflectionHelper = new ReflectionHelper;
        $object = $ReflectionHelper->createInstance(
            $objectClassName,
            [
              null,
              null,
              new Database($this->mockClient)
            ]
        );
        return $object;
    }
}
