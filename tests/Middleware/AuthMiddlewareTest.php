<?php
namespace Tests\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Exceptions\CTAException;
use App\Models\TokenModel;
use App\Http\Middleware\AuthMiddleware;
use Mockery;

class AuthMiddlewareTest extends \Tests\TestCase
{
    public function testHandleTokenFound()
    {
        $middleware = new AuthMiddleware;
        $tokenModel = Mockery::mock(TokenModel::class);
        $token = 'TOKEN';
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->andReturn($token);
        $time = time();
        $expTime = 600;
        $jwt = json_decode(json_encode([
            'iss' => config('jwt.issuer'),
            'iat' => $time,
            'exp' => $time + $expTime,
            'uid' => 1,
        ]));
        $tokenModel
            ->shouldReceive('decode')
            ->once()
            ->andReturn($jwt);
        $request = new Request([], [], [], [], [], [], null);
        $next = function () {
            return new JsonResponse;
        };
        $response = $middleware->handle($request, $next, $tokenModel);
        $this->assertEquals(200, $response->status());
    }

    public function testHandleTokenNotFound()
    {
        $middleware = new AuthMiddleware;
        $tokenModel = Mockery::mock(TokenModel::class);
        $token = null;
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->andReturn($token);
        $tokenModel
            ->shouldReceive('decode')
            ->never();
        $request = new Request([], [], [], [], [], [], null);
        $next = function () {
            return new JsonResponse;
        };
        $response = $middleware->handle($request, $next, $tokenModel);
        $this->assertEquals(401, $response->status());
    }

    public function testHandleTokenInvalid()
    {
        $middleware = new AuthMiddleware;
        $tokenModel = Mockery::mock(TokenModel::class);
        $token = 'HASTOKEN';
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->andReturn($token);
        $tokenModel
            ->shouldReceive('decode')
            ->once()
            ->andThrow(\Exception::class, 'EXCEPTION');
        $request = new Request([], [], [], [], [], [], null);
        $next = function () {
            return new JsonResponse;
        };
        $response = $middleware->handle($request, $next, $tokenModel);
        $this->assertEquals(401, $response->status());
    }

    public function testHandleTokenAutoRefresh()
    {
        $middleware = new AuthMiddleware;
        $tokenModel = Mockery::mock(TokenModel::class);
        $token = 'TOKEN';
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->andReturn($token);
        $time = time();
        $expTime = 60;
        $jwt = json_decode(json_encode([
            'iss' => config('jwt.issuer'),
            'iat' => $time,
            'exp' => $time + $expTime,
            'uid' => 1,
        ]));
        $tokenModel
            ->shouldReceive('decode')
            ->once()
            ->andReturn($jwt);
        $request = new Request([], [], [], [], [], [], null);
        $next = function () {
            return new JsonResponse;
        };
        $response = $middleware->handle($request, $next, $tokenModel);
        $cookieJar = app()->make('cookie');
        $hasQueued = $cookieJar->hasQueued(config('jwt.cookie_name'));
        $this->assertEquals(200, $response->status());
        $this->assertTrue($hasQueued);
    }
}
