<?php
namespace Tests\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Exceptions\CTAException;
use App\Http\Middleware\CookieMiddleware;
use Mockery;

class CookieMiddlewareTest extends \Tests\TestCase
{
    public function testHandleWithCookie()
    {
        $middleware = new CookieMiddleware;
        $request = new Request([], [], [], [], [], [], null);
        $next = function () {
            return new JsonResponse;
        };
        $cookieJar = app()->make('cookie');
        $cookie = $cookieJar->make(
            'COOKIENAME',
            'COOKIEVALUE',
            0,
            'COOKIEPATH',
            'COOKIEHOST'
        );
        $cookieJar->queue($cookie);
        $response = $middleware->handle($request, $next);
        $class = new \ReflectionClass($response->headers);
        $property = $class->getProperty('cookies');
        $property->setAccessible(true);
        $cookies = $property->getValue($response->headers);
        $this->assertArrayHasKey('COOKIEHOST', $cookies);
        $this->assertArrayHasKey('COOKIEPATH', $cookies['COOKIEHOST']);
        $this->assertArrayHasKey('COOKIENAME', $cookies['COOKIEHOST']['COOKIEPATH']);
    }
}
