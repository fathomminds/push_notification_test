<?php
namespace Tests\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\CTAException;
use App\Http\Controllers\AuthController;
use App\Models\UserModel;
use App\Models\TokenModel;
use Mockery;

class AuthControllerTest extends \Tests\TestCase
{
    public function testLoginSuccess()
    {
        $userModel = Mockery::mock(UserModel::class);
        $tokenModel = Mockery::mock(TokenModel::class);
        $controller = new AuthController;
        $token = 'JWT';
        $userModel
            ->shouldReceive('login')
            ->once()
            ->andReturn($token);
        $tokenModel
            ->shouldReceive('setCookie')
            ->once();
        $input = [
            'email' => '',
            'password' => '',
        ];
        $request = new Request([], [], [], [], [], [], json_encode($input));
        $response = $controller->login($userModel, $tokenModel, $request);
        $expectedContent = json_encode(
            [
                'jwt_token' => $token
            ]
        );
        $this->assertEquals($expectedContent, $response->getContent());
        $this->assertEquals(200, $response->status());
    }

    public function testLoginFailure()
    {
        $userModel = Mockery::mock(UserModel::class);
        $tokenModel = Mockery::mock(TokenModel::class);
        $controller = new AuthController;
        $token = 'JWT';
        $userModel
            ->shouldReceive('login')
            ->once()
            ->andThrow(CTAException::class, 'LOGIN FAILURE');
        $tokenModel
            ->shouldReceive('setCookie')
            ->never();
        $input = [
            'email' => '',
            'password' => '',
        ];
        $request = new Request([], [], [], [], [], [], json_encode($input));
        $response = $controller->login($userModel, $tokenModel, $request);
        $expectedContent = json_encode('Unauthorized');
        $this->assertEquals($expectedContent, $response->getContent());
        $this->assertEquals(401, $response->status());
    }

    public function testCheckSuccess()
    {
        $tokenModel = Mockery::mock(TokenModel::class);
        $controller = new AuthController;
        $request = new Request([], [], [], [], [], [], null);
        $jwt = 'JWT OBJECT';
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->with($request)
            ->andReturn($jwt);
        $token = 'JWT';
        $tokenModel
            ->shouldReceive('decode')
            ->with($jwt, config('jwt.secret'), config('jwt.allowed_algs'))
            ->once()
            ->andReturn($token);
        $response = $controller->check($tokenModel, $request);
        $expectedContent = json_encode(
            [
                'decoded_jwt_token' => $token
            ]
        );
        $this->assertEquals($expectedContent, $response->getContent());
        $this->assertEquals(200, $response->status());
    }

    public function testCheckFailure()
    {
        $tokenModel = Mockery::mock(TokenModel::class);
        $controller = new AuthController;
        $request = new Request([], [], [], [], [], [], null);
        $jwt = 'JWT OBJECT';
        $tokenModel
            ->shouldReceive('get')
            ->once()
            ->with($request)
            ->andReturn($jwt);
        $token = 'JWT';
        $tokenModel
            ->shouldReceive('decode')
            ->andThrow(\Exception::class, 'EXCEPTION');
        $response = $controller->check($tokenModel, $request);
        $expectedContent = json_encode(
            [
                'error' => 'EXCEPTION'
            ]
        );
        $this->assertEquals($expectedContent, $response->getContent());
        $this->assertEquals(200, $response->status());
    }
}
