<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/', 'SiteController@index');

$app->group(['prefix' => 'test'], function () use ($app) {
    $app->get('get', ['uses' => 'TestController@testGet']);
    $app->post('post', ['uses' => 'TestController@testPost']);
    $app->put('put', ['uses' => 'TestController@testPut']);
    $app->group(['prefix' => 'authenticated'], function () use ($app) {
        $app->get('get', ['uses' => 'TestController@testAuthenticatedGet']);
        $app->post('post', ['uses' => 'TestController@testAuthenticatedPost']);
        $app->put('put', ['uses' => 'TestController@testAuthenticatedPut']);
    });
});

$app->get('/hb', 'TestController@hb');
$app->get('/alarms', 'TestController@list');
$app->get('/alarms/templates', 'TestController@listTemplates');
$app->post('/alarms/{id}', 'TestController@newAlarm');
$app->put('/alarms', 'TestController@setSeen');
$app->put('/set-all-status', 'TestController@setAllStatus');

$app->post('/mandown', 'TestController@mandown');

$app->post('/log/errors', 'TestController@errorLog');

$app->get('/auth', 'TestController@auth');
$app->post('register', ['uses' => 'RegisterController@register']);
$app->get('/register/{deviceId}', 'TestController@register');
$app->get('/config', 'TestController@config');

$app->group(['prefix' => 'auth'], function () use ($app) {
    $app->post('login', 'AuthController@login');
});

$app->group(['prefix' => 'device'], function () use ($app) {
    $app->post('set', ['uses' => 'DevicesController@set']);
    $app->post('delete', ['middleware' => 'auth', 'uses' => 'DevicesController@delete']);
    $app->get('list', ['middleware' => 'auth', 'uses' => 'DevicesController@list']);
    $app->post('send', ['uses' => 'DevicesController@send']);
});

$app->group(['prefix' => 'v1'], function() use ($app) {
    $app->get('auth', 'AuthController@auth');
    $app->get('config', 'ProxyController@proxy');
    $app->get('alarms', 'ProxyController@proxy');
    $app->get('alarms/templates', 'ProxyController@proxy');
    $app->post('alarms/{id}', 'ProxyController@proxy');
    $app->put('alarms', 'ProxyController@proxy');
    $app->post('register', ['uses' => 'RegisterController@registerProxied']);
    $app->post('mandown', 'MandownController@send');
    $app->get('hb', 'ProxyController@proxy');
    $app->post('log/errors', 'ErrorReportController@send');
});

$app->get('/sleep', function() use ($app) {
    sleep(5);
});