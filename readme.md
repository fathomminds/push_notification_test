# TODO API

Based on Clurexi API Seed

## Setup

1. Use this repository with tododocker
2. Clone this repo as `api` next to the `docker` folder, under a project folder in C:\\Users
3. Start the docker containers
4. Run `composer install` in the workspace docker container
5. Create a file `.env` in the `api` folder, and provide the Clusterpoint database info and other Environment Variables listed in `.env.example`

## Contribute

* Use PSR2 Code style
* Use [GitFlow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow)
* Write Unit tests
* Create Pull requests for merging into `develop`, `master` and `staging` branches

## Continous Integration

All pushed branches goes through the Bitbucket Pipelines, which will checks the PSR2 syntax and the unit tests.  
You can check these on your own:

1. Login to the workspace docker container
    * run the `phpunit` command to run unit tests
    * run the `phpcs --standard=PSR2 --colors --ignore=vendor,storage .` for PSR2 check. *Use `--report=summary` option for a short summary*
  
## Notes

* Constants declared in the `.env` file, can be accessible like environment variables.
* nginx logs can be found in docker/logs/nginx folder