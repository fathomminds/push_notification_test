<!DOCTYPE html>
<html>
<head>
  <title>Beralarm Mobile App</title>
  <style>
    table {
      border-collapse: collapse;
    }
    td, th {
      border: 1px solid #000;
      padding: 3px;
    }
  </style>
</head>
<body>
<h1>Beralarm Mobile Application - Authorization</h1>
<div id='loginWrapper'>
  <label for='email'>Email</label>
  <input id='email' type='text' name='email' value='' /><br />
  <label for='password'>Password</label>
  <input id='password' type='password' name='password' value='' /><br />
  <button id='login'>Login</button>
</div>
<br />
<br />
<hr />

<div id='content' style='display: none;'>
  <h2>Configure notifications</h2>
  <textarea id='notifications' style='width: 100%; height: 300px;'></textarea>
  <button id='saveNotifications'>Save</button>
  <br />
  <br />
  <hr />

  <h2>Set device ID for user</h2>
  <label for='name'>Name</label>
  <input id='name' name='name' type='text' /><br />
  <label for='platform'>Platform</label>
  <input id='platform' name='platform' type='text' placeholder='android/ios' /><br />
  <label for='device-id'>Device ID</label>
  <input id='device-id' name='device-id' type='text' /><br />
  <button id='device-set'>Set device</button>
  <br />
  <br />
  <hr />

  <h2>List of available devices</h2>
  <button id='device-list'>Device list</button>
  <table id='devices'>
      <thead>
          <tr>
              <th>_id</th>
              <th>Name</th>
              <th>Plaform</th>
              <th>Device ID</th>
              <th>Endpoint</th>
              <th>Actions</th>
          </tr>
      </thead>
      <tbody></tbody>
  </table>
  <br />
  <br />
  <hr />

</div>
<script>
(function(global){
  "use strict";
  global.$ = document.querySelector.bind(document);
  global.Api = {
      createReq: function (success, error) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === XMLHttpRequest.DONE ) {
               if (xmlhttp.status === 200) {
                  success(JSON.parse(xmlhttp.responseText));
               }
               else if (xmlhttp.status === 400) {
                  error(xmlhttp.status, xmlhttp.statusText, xmlhttp.responseText);
               }
               else {
                  error(xmlhttp.status, xmlhttp.statusText, xmlhttp.responseText);
               }
            }
        };
        return xmlhttp;
      },
      call: function (params) {
        if (typeof params.method === 'undefined') params.method = 'GET';
        if (typeof params.url === 'undefined') params.url = '/';
        if (typeof params.success === 'undefined') params.success = function () {};
        if (typeof params.error === 'undefined') params.error = function () {};
        if (typeof params.data === 'undefined') params.data ={};
        var data = JSON.stringify(params.data);
        if (data === '{}') data = null;
        var xmlhttp = this.createReq(params.success, params.error);
        xmlhttp.open(params.method, params.url, true);
        xmlhttp.send(data);
      }
  }
})(window);
(function () {
"use strict";
$('#login').onclick = function () {
    Api.call({
        method: 'POST',
        url: '/auth/login',
        success: function (data) {
          $('#content').style.display = 'block';
          window.listDevices();
        },
        error: function (s, st, t) {
            if (s === 401) {
              $('#content').style.display = 'none';
            }
        },
        data: {
            email: $('#email').value,
            password: $('#password').value,
        }
    });
}

$('#device-set').onclick = function () {
  Api.call({
      method: 'POST',
      url: '/device/set',
      success: function (data) {
        window.deviceRow(data);
      },
      data: {
          name: $('#name').value,
          platform: $('#platform').value,
          deviceId: $('#device-id').value,
      }
  });
}

window.listDevices = function () {
  Api.call({
      method: 'GET',
      url: '/device/list',
      success: function (data) {
          $('#devices tbody').innerHTML = '';
          for (var idx in data) {
            window.deviceRow(data[idx]);
          }
      },
      data: {}
  });
}

window.deviceRow = function (device) {
  if ($('#'+device._id)) {
    var append = false;
    var tr = $('#'+device._id);
  } else {
    var append = true;
    var tr = document.createElement('TR');
  }
  tr.setAttribute('data-device', JSON.stringify(device));
  tr.setAttribute('id', device._id);
  var actions = {
      delete:                 '<button onclick="deleteDevice(JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">DELETE</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      REGISTRATION_ACCEPTED:  '<button onclick="sns(\'REGISTRATION_ACCEPTED\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">REGISTRATION_ACCEPTED</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      REGISTRATION_DENIED:    '<button onclick="sns(\'REGISTRATION_DENIED\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">REGISTRATION_DENIED</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      DEREGISTERED:           '<button onclick="sns(\'DEREGISTERED\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">DEREGISTERED</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      REFRESH_TOKEN:          '<button onclick="sns(\'REFRESH_TOKEN\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">REFRESH_TOKEN</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      LOAD_CONFIGURATION:     '<button onclick="sns(\'LOAD_CONFIGURATION\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">LOAD_CONFIGURATION</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      ALARM:                  '<button onclick="sns(\'ALARM\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">ALARM</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      REFRESH_ALARM:          '<button onclick="sns(\'REFRESH_ALARM\', JSON.parse(this.parentNode.parentNode.getAttribute(\'data-device\')));">REFRESH_ALARM</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
  }
  var actionsHTML = '<td>' +
    actions.delete +
    actions.REGISTRATION_ACCEPTED +
    actions.REGISTRATION_DENIED +
    actions.DEREGISTERED +
    actions.REFRESH_TOKEN +
    actions.LOAD_CONFIGURATION +
    actions.ALARM +
    actions.REFRESH_ALARM +
  '</td>';
  tr.innerHTML = '<td>'+device._id+'</td><td>'+device.name+'</td><td>'+device.platform+'</td><td><span title="'+device.deviceId+'">Hover</span></td><td><span title="'+device.endpoint+'">Hover</span></td>'+actionsHTML;
  if (append) $('#devices tbody').appendChild(tr);
}

window.deleteDevice = function (device) {
  Api.call({
      method: 'POST',
      url: '/device/delete',
      success: function (data) {
        var tr = $('#'+data._id);
        tr.parentNode.removeChild(tr);
      },
      data: device
  });
}

window.PAYLOADS = {
  android: {
    REGISTRATION_ACCEPTED:  {GCM: {data:{notId: 1, notificationType: "REGISTRATION_ACCEPTED", body: "Registration accepted", title: "REGISTRATION_ACCEPTED"}}},
    REGISTRATION_DENIED:    {GCM: {data:{notId: 1, notificationType: "REGISTRATION_DENIED", body: "Registration denied", title: "REGISTRATION_DENIED"}}},
    DEREGISTERED:           {GCM: {data:{notId: 1, notificationType: "DEREGISTERED", body: "Device deregistered", title: "DEREGISTERED"}}},
    REFRESH_TOKEN:          {GCM: {data:{notId: 1, notificationType: "REFRESH_TOKEN", body: "Refresh token", title: "REFRESH_TOKEN"}}},
    LOAD_CONFIGURATION:     {GCM: {data:{notId: 1, notificationType: "LOAD_CONFIGURATION", body: "Load configuration", title: "LOAD_CONFIGURATION"}}},
    ALARM:                  {GCM: {data:{notId: 1002, notificationType: "ALARM", body: "Alarm", title: "ALARM", alarmId: 1002}}},
    REFRESH_ALARM:          {GCM: {data:{notId: 1, notificationType: "REFRESH_ALARM", body: "Refresh alarm", title: "REFRESH_ALARM"}}},
  },
  ios: {
    REGISTRATION_ACCEPTED:  {APNS: {aps:{notificationType: "REGISTRATION_ACCEPTED", alert: "Registration accepted", sound: "default", "content-available": 1}, notId: 1}},
    REGISTRATION_DENIED:    {APNS: {aps:{notificationType: "REGISTRATION_DENIED", alert: "Registration denied", sound: "default", "content-available": 1}, notId: 1}},
    DEREGISTERED:           {APNS: {aps:{notificationType: "DEREGISTERED", alert: "Device deregistered", sound: "default", "content-available": 1}, notId: 1}},
    REFRESH_TOKEN:          {APNS: {aps:{notificationType: "REFRESH_TOKEN", alert: "Refresh token", sound: "default", "content-available": 1}, notId: 1}},
    LOAD_CONFIGURATION:     {APNS: {aps:{notificationType: "LOAD_CONFIGURATION", alert: "Load configuration", sound: "default", "content-available": 1}, notId: 1}},
    ALARM:                  {APNS: {aps:{notificationType: "ALARM", alert: "Alarm", sound: "default", "content-available": 1, alarmId: 1002}, notId: 1002}},
    REFRESH_ALARM:          {APNS: {aps:{notificationType: "REFRESH_ALARM", alert: "Refresh alarm", sound: "default", "content-available": 1}, notId: 1}},
  }
}

$('#notifications').value = JSON.stringify(window.PAYLOADS, null, 2);
$('#saveNotifications').onclick = function () {
    window.PAYLOADS = JSON.parse($('#notifications').value);
}

window.sns = function (type, device) {
  Api.call({
      method: 'POST',
      url: '/device/send',
      success: function (data) {
        console.log('Message published: ' + data.messageId);
      },
      data: {
        device: device,
        payload: PAYLOADS[device.platform][type]
      }
  });
}
})();
</script>
</body>
</html>
