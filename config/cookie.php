<?php

return  [
    'exp_type_session' => 3000,
    'path' => '/',
    'host' => env('COOKIE_HOST'),
     // 'secure' => true,
    'secure' => false,  // just temporary solution
    'not_secure' => false,
    'http_only' => true,
    'not_http_only' => false,
];
