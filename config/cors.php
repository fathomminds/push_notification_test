<?php

return  [
    'allow_origins' => explode(',', env('CORS_ALLOW_ORIGIN_LIST')),
    'allow_methods' => [
      'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'
    ],
    'allow_headers' => [
        'content-type',
        'Authorization',
    ],
    'allow_credentials' => true,
];
