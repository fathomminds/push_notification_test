<?php

return  [
    'cookie_name' => env('JWT_COOKIE_NAME'),
    'secret' => env('JWT_SECRET'),
    'issuer' => env('JWT_ISSUER'),
    'exp_type_session' => 3000,
    'allowed_algs' => ['HS256'],
    'auto_refresh_threshold' => 300,
];
